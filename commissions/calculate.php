<?php
/* Copyright (C) 2012	Christophe Battarel	<christophe.battarel@altairis.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/commissions/index.php
 *	\ingroup    commissions
 *	\brief      Page des commissions par agent commercial
 */
ini_set('max_execution_time', 300); //@TODO OPTIMISER CODE
$res=@include("../main.inc.php");					// For root directory
if (! $res) $res=@include("../../main.inc.php");		// For "custom" directory

if (!$user->rights->commissions->lire && !$user->rights->commissions->liretous) accessforbidden();

require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';
if ($conf->global->COMMISSION_METHOD == "ORDER")
	require_once DOL_DOCUMENT_ROOT.'/commande/class/commande.class.php';
else
	require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/facture.class.php';
require_once DOL_DOCUMENT_ROOT.'/product/class/product.class.php';
if (! empty($conf->margin->enabled))
	require_once DOL_DOCUMENT_ROOT.'/margin/lib/margins.lib.php';

$langs->load("companies");
$langs->load("bills");
$langs->load("products");
$langs->load("commissions@commissions");
if (! empty($conf->margin->enabled))
	$langs->load("margins");

$arrayfields=array(
    'customer'=>array('label'=>$langs->trans("Customer"), 'checked'=>1),
	'label_categorie_societe'=>array('label'=>$langs->trans("CustCateg"), 'checked'=>1),
    'invoiceref'=>array('label'=>$langs->trans("InvoiceRef"), 'checked'=>1),
    'product'=>array('label'=>$langs->trans("Product"), 'checked'=>1),
    'invoicedate'=>array('label'=>$langs->trans("InvoiceDate"), 'checked'=>1),
    'invoiceamountht'=>array('label'=>$langs->trans("InvoiceAmountHT"), 'checked'=>1),
    'invoiceamountttc'=>array('label'=>$langs->trans("InvoiceAmountTTC"), 'checked'=>1),
    'paymentdate'=>array('label'=>$langs->trans("PaymentDateComm"), 'checked'=>1),
    'paymentamount'=>array('label'=>$langs->trans("PaymentAmount"), 'checked'=>1),
    'commercialagent'=>array('label'=>$langs->trans("CommercialAgent"), 'checked'=>1),
    'productbase'=>array('label'=>$langs->trans("ProductBase"), 'checked'=>1),
    'productrate'=>array('label'=>$langs->trans("ProductRate"), 'checked'=>1),
    'productcommission'=>array('label'=>$langs->trans("ProductCommission"), 'checked'=>1),
	 'servicebase'=>array('label'=>$langs->trans("ServiceBase"), 'checked'=>1),
	 'servicerate'=>array('label'=>$langs->trans("ServiceRate"), 'checked'=>1),

    'servicecommission'=>array('label'=>$langs->trans("ServiceCommission"), 'checked'=>1),
    'totalcommission'=>array('label'=>$langs->trans("TotalCommission"), 'checked'=>1),
   
);
include DOL_DOCUMENT_ROOT.'/core/actions_changeselectedfields.inc.php';
// Security check
$apid = GETPOST('agentid','int');
$agentid = ($user->rights->commissions->liretous)?(!empty($apid)?$apid:-1):$user->id;
$detail = $agentid > 0?GETPOST('detail'):'';

$hookmanager->initHooks(array('commissionlist'));

// chargement des règles de commission
$commissionRules = getCommissionRules();

$mesg = '';

$startdate=$enddate='';

if (!empty($_POST['startdatemonth']))
  $startdate = date('Y-m-d', dol_mktime(12, 0, 0, $_POST['startdatemonth'],  $_POST['startdateday'],  $_POST['startdateyear']));
else
	$startdate = date('Y-m-d', dol_mktime(12, 0, 0, date('m'), 1,  date('Y')));
if (!empty($_POST['enddatemonth']))
  $enddate = date('Y-m-d', mktime(12, 0, 0, $_POST['enddatemonth'],  $_POST['enddateday'],  $_POST['enddateyear']));
else
	$enddate = date('Y-m-d', mktime(12, 0, 0, date('m')+1, 0,  date('Y')));

/*
 * View
 */

$userstatic = new User($db);
$companystatic = new Societe($db);
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$docstatic=new Commande($db);
else
	$docstatic=new Facture($db);

$form = new Form($db);

llxHeader('',$langs->trans("Commissions"));

$text=$langs->trans("Commissions");
print_fiche_titre($text);

print '<br/>';
print '<form method="POST" id="searchFormList" action="'.$_SERVER["PHP_SELF"].'">';
    if ($optioncss != '') print '<input type="hidden" name="optioncss" value="'.$optioncss.'">';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	print '<input type="hidden" name="formfilteraction" id="formfilteraction" value="list">';
	print '<input type="hidden" name="action" value="list">';
	print '<input type="hidden" name="sortfield" value="'.$sortfield.'">';
	print '<input type="hidden" name="sortorder" value="'.$sortorder.'">';
	print '<input type="hidden" name="page" value="'.$page.'">';
print '<form method="post" name="sel">';
print '<table class="border" width="100%">';

print '<tr><td>'.$langs->trans('CommercialAgent').'</td>';
print '<td>';
if ($user->rights->commissions->liretous)
	print $form->select_dolusers($agentid,'agentid',1,'',0,'','');
else
{
	print $user->firstname.'&nbsp;'.$user->lastname;
}
print '</td>';
print '<td colspan="3">';
if ($agentid > 0)
{
	print '<input type="radio" name="detail" value="0" ';
	if (empty($detail))
	    print 'checked=checked ';
	print '/>&nbsp;';
	print $langs->trans('CustomerView').'&nbsp;&nbsp;';
	print '<input type="radio" name="detail" value="1" ';
	if (!empty($detail))
	    print 'checked=checked ';
	print '/>&nbsp;';
	if ($conf->global->COMMISSION_METHOD == "ORDER")
		print $langs->trans('OrderView');
	else
		print $langs->trans('InvoiceView');
}
else print '&nbsp;';
print '</td></tr>';

// Commission period
print '<tr>';
print '<td>'.$langs->trans('CommissionMonth').'</td>';
print '<td><div class="selMonth"></div></td>';
print '<td>'.$langs->trans('StartDate').'&nbsp;&nbsp;';
$form->select_date($startdate,'startdate','','',1,"sel",1,1);
print '</td>';
print '<td>'.$langs->trans('EndDate').'&nbsp;&nbsp;';
$form->select_date($enddate,'enddate','','',1,"sel",1,1);
print '</td>';
print '<td style="text-align: center;">';
print '<input class="button" type="submit" value="'.$langs->trans('Launch').'" />';
print '</td></tr>';
print "</table>";
//print '</form>';
//TODO Optimiser code
$sql = "SELECT DISTINCT s.nom, s.rowid as socid, prod.rowid as productid, s.code_client, s.client, sc.fk_user as commercial, e.fk_socpeople as contact, u.login, u.rowid as agentid, e.fk_c_type_contact as fk_contact_type,d.rowid as lineid,"
	. "(SELECT GROUP_CONCAT(fk_categorie) FROM ".MAIN_DB_PREFIX."categorie_societe WHERE fk_soc=s.rowid) as categorie_societe,"
	. "(SELECT GROUP_CONCAT(c.label) FROM ".MAIN_DB_PREFIX."categorie as c LEFT JOIN ".MAIN_DB_PREFIX."categorie_societe cs ON (cs.fk_categorie = c.rowid) WHERE cs.fk_soc=s.rowid) as label_categorie_societe,"
	. "(SELECT GROUP_CONCAT(fk_categorie) FROM ".MAIN_DB_PREFIX."categorie_product WHERE fk_product=prod.rowid) as categorie_product,";
	

if ($conf->global->COMMISSION_METHOD == "ORDER")
	$sql.= " f.total_ht as total, f.date_commande as datef, f.rowid as facid, f.ref, f.total_ttc";
else
	$sql.= " f.total, f.datef, f.rowid as facid, f.facnumber as ref, f.total_ttc";
if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT") {
	$sql .= ", p.rowid as payment_id, p.datep, pf.amount as payment";
	if ($conf->global->COMMISSION_BASE == "TURNOVER") {  // CA /réglements
		$sql.= ", sum(case d.product_type when 1 then 0 else (pf.amount * d.total_ht / f.total_ttc) end)  as productBase" ;
		$sql.= ", sum(case d.product_type when 1 then (pf.amount * d.total_ht / f.total_ttc) else 0 end) as serviceBase" ;
	}
	elseif ($conf->global->COMMISSION_BASE == "MARGIN") {
		$sql.= ", sum(case d.product_type when 1 then 0 else (pf.amount * (".$db->ifsql('d.total_ht <=0','-1 * (abs(d.total_ht) - (d.buy_price_ht * d.qty))','d.total_ht - (d.buy_price_ht * d.qty)').") / f.total_ttc) end)  as productBase" ;
		$sql.= ", sum(case d.product_type when 1 then (pf.amount * (".$db->ifsql('d.total_ht <=0','-1 * (abs(d.total_ht) - (d.buy_price_ht * d.qty))','d.total_ht - (d.buy_price_ht * d.qty)').") / f.total_ttc) else 0 end) as serviceBase" ;
	}
}
else
{
	if ($conf->global->COMMISSION_BASE == "TURNOVER") {
		$sql.= ", sum(case d.product_type when 1 then 0 else d.total_ht end)  as productBase" ;
		$sql.= ", sum(case d.product_type when 1 then d.total_ht else 0 end) as serviceBase" ;
	}
	elseif ($conf->global->COMMISSION_BASE == "MARGIN") {
		$sql.= ", sum(case d.product_type when 1 then 0 else (".$db->ifsql('d.total_ht <=0','-1 * (abs(d.total_ht) - (d.buy_price_ht * d.qty))','d.total_ht - (d.buy_price_ht * d.qty)').") end)  as productBase" ;
		$sql.= ", sum(case d.product_type when 1 then (".$db->ifsql('d.total_ht <=0','-1 * (abs(d.total_ht) - (d.buy_price_ht * d.qty))','d.total_ht - (d.buy_price_ht * d.qty)').") else 0 end) as serviceBase" ;
	}
}
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$sql.= " FROM ".MAIN_DB_PREFIX."commande f";
else
	$sql.= " FROM ".MAIN_DB_PREFIX."facture f";
$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."societe s ON f.fk_soc = s.rowid AND s.entity = ".$conf->entity;
//$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."categorie_societe cats ON cats.fk_soc = s.rowid";


if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
{
    $sql .= " LEFT JOIN ".MAIN_DB_PREFIX."paiement_facture pf on pf.fk_facture = f.rowid ";
    $sql .= " LEFT JOIN ".MAIN_DB_PREFIX."paiement p on p.rowid = pf.fk_paiement";
}
$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."societe_commerciaux as sc ON sc.fk_soc = f.fk_soc";
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."contact_commande e ON e.element_id = f.rowid";
else
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."contact_facture e ON e.element_id = f.rowid";
$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."user as u ON (u.rowid IN (".implode(",",getUserNotLinked()).",sc.fk_user,e.fk_socpeople)) ";
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."commandedet as d ON d.fk_commande = f.rowid";
else
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."facturedet as d ON d.fk_facture = f.rowid";
$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as prod ON d.fk_product = prod.rowid";
//$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."categorie_product as catp ON catp.fk_product = prod.rowid";
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$sql.= " WHERE f.fk_statut = 3";
elseif ($conf->global->COMMISSION_METHOD == "FULLPAYMENT")
	$sql.= " WHERE f.fk_statut >= 2";
else
	$sql.= " WHERE f.fk_statut > 0";
$sql.= " AND f.entity = ".$conf->entity;
$sql.= " AND (d.product_type = 0 OR d.product_type = 1)";
if ($agentid > 0) {
	$sql.= " AND u.rowid = ".$agentid;
}
else
	$sql.= "  AND u.rowid IS NOT NULL";
if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
{
	if (!empty($startdate))
	  $sql.= " AND DATE(p.datep) >= '".$startdate."'";
	if (!empty($enddate))
	  $sql.= " AND DATE(p.datep) <= '".$enddate."'";
}
elseif ($conf->global->COMMISSION_METHOD == "INVOICE")
{
	if (!empty($startdate))
	  $sql.= " AND DATE(f.datef) >= '".$startdate."'";
	if (!empty($enddate))
	  $sql.= " AND DATE(f.datef) <= '".$enddate."'";
}
elseif ($conf->global->COMMISSION_METHOD == "ORDER")
{
	if (!empty($startdate))
		$sql.= " AND DATE(f.date_commande) >= '".$startdate."'";
	if (!empty($enddate))
		$sql.= " AND DATE(f.date_commande) <= '".$enddate."'";
}

if ($conf->global->COMMISSION_BASE == "MARGIN")
	$sql .= " AND d.buy_price_ht IS NOT NULL";
if (($conf->global->COMMISSION_BASE == "MARGIN") && isset($conf->global->ForceBuyingPriceIfNull) && $conf->global->ForceBuyingPriceIfNull == 1)
	$sql .= " AND d.buy_price_ht <> 0";

$select = $sql;

$groupby = " GROUP BY s.nom, s.rowid, s.code_client, s.client, sc.fk_user, e.fk_socpeople, u.login, u.rowid, e.fk_c_type_contact,d.rowid,prod.rowid,";
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$groupby.= " f.total_ht, f.date_commande, f.rowid, f.ref, f.total_ttc";
else
	$groupby.= " f.total, f.datef, f.rowid, f.facnumber, f.total_ttc";
if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
	$groupby .= ", p.rowid, p.datep, pf.amount";

$sql.= $groupby;

$sql.= " ORDER BY u.login, u.rowid, s.nom, s.rowid, d.rowid,";
if ($conf->global->COMMISSION_METHOD == "ORDER")
	$sql.= " f.date_commande, f.rowid";
else
	$sql.= " f.datef, f.rowid";
if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
	$sql .= ", p.datep, p.rowid";
$sql.= ", e.fk_socpeople desc";

$result = $db->query($sql);
if ($result)
{
	$num = $db->num_rows($result);

	print '<br>';
	if ($agentid > 0) {
	    $agent = new User($db);
	    $agent->fetch($agentid);
		print '<b>'.$langs->trans("CommissionDetailsFor",	$agent->firstname.' '.$agent->lastname).'</b>';
	}
	
	$i = 0;
	print "<table class=\"noborder  liste\" border='1' width=\"100%\">";

	print '<tr class="liste_titre">';
	$varpage=empty($contextpage)?$_SERVER["PHP_SELF"]:$contextpage;
 $selectedfields=$form->multiSelectArrayWithCheckbox('selectedfields', $arrayfields, $varpage);	// This also change content of $arrayfields
	if ($agentid > 0)
	{
	
		
		if(!empty($arrayfields['customer']['checked']))print_liste_field_titre($langs->trans("Customer"));
		if(!empty($arrayfields['label_categorie_societe']['checked']))print_liste_field_titre($langs->trans("CustCateg"));
		if (!empty($detail))
		{
			if(!empty($arrayfields['invoiceref']['checked']))print_liste_field_titre($langs->trans("InvoiceRef"));
			if(!empty($arrayfields['product']['checked']))print_liste_field_titre($langs->trans("Product"),null, null, null, null,'align="center"');
			if(!empty($arrayfields['invoicedate']['checked']))print_liste_field_titre($langs->trans("InvoiceDate"),null, null, null, null,'align="center"');
			if(!empty($arrayfields['invoiceamountht']['checked']))print_liste_field_titre($langs->trans("InvoiceAmountHT"),null, null, null, null,'align="right"');
			
			if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
			{
				if(!empty($arrayfields['invoiceamountttc']['checked']))print_liste_field_titre($langs->trans("InvoiceAmountTTC"),null, null, null, null,'align="right"');
				if(!empty($arrayfields['paymentdate']['checked']))print_liste_field_titre($langs->trans("PaymentDateComm"),null, null, null, null,'align="center"');
				if(!empty($arrayfields['paymentamount']['checked']))print_liste_field_titre($langs->trans("PaymentAmount"),null, null, null, null,'align="right"');
			}
		}
		else
		{
        	if(!empty($arrayfields['invoiceamountht']['checked']))print_liste_field_titre($langs->trans("InvoiceAmountHT"),null, null, null, null,'align="right"');
            if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
	        {
				if(!empty($arrayfields['invoiceamountttc']['checked']))print_liste_field_titre($langs->trans("InvoiceAmountTTC"),null, null, null, null,'align="right"');
            	if(!empty($arrayfields['paymentamount']['checked']))print_liste_field_titre($langs->trans("PaymentAmount"),null, null, null, null,'align="right"');
			}
		}
	}
	else
	{
		if(!empty($arrayfields['commercialagent']['checked']))print_liste_field_titre($langs->trans("CommercialAgent"));
		if(!empty($arrayfields['invoiceamountht']['checked']))print_liste_field_titre($langs->trans("InvoiceAmountHT"),null, null, null, null,'align="right"');
        if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
		{
			if(!empty($arrayfields['invoiceamountttc']['checked']))print_liste_field_titre($langs->trans("InvoiceAmountTTC"),null, null, null, null,'align="right"');
        	if(!empty($arrayfields['paymentamount']['checked']))print_liste_field_titre($langs->trans("PaymentAmount"),null, null, null, null,'align="right"');
		}
	}

	// product commission
	if(!empty($arrayfields['productbase']['checked']))print_liste_field_titre($langs->trans("ProductBase"),null, null, null, null,'align="right"');

	if (!empty($detail))
		if(!empty($arrayfields['productrate']['checked']))print_liste_field_titre($langs->trans("ProductRate"),null, null, null, null,'align="right"');
	if(!empty($arrayfields['productcommission']['checked']))print_liste_field_titre($langs->trans("ProductCommission"),null, null, null, null,'align="right"');

	// service commission
	if(!empty($arrayfields['servicebase']['checked']))print_liste_field_titre($langs->trans("ServiceBase"),null, null, null, null,'align="right"');

	if (!empty($detail))
		if(!empty($arrayfields['servicerate']['checked']))print_liste_field_titre($langs->trans("ServiceRate"),null, null, null, null,'align="right"');
	if(!empty($arrayfields['servicecommission']['checked']))print_liste_field_titre($langs->trans("ServiceCommission"),null, null, null, null,'align="right"');

	// total commission
	if(!empty($arrayfields['totalcommission']['checked']))print_liste_field_titre($langs->trans("TotalCommission"),null, null, null, null,'align="right"');
	
 	print_liste_field_titre($selectedfields, $_SERVER["PHP_SELF"],"",'','','align="center"',$sortfield,$sortorder,'maxwidthsearch ');

	print "</tr>\n";

	$cumul_commissions_produit = 0;
	$cumul_commissions_service = 0;
	$cumul_factures_ht = 0;
	$cumul_factures_ttc = 0;
	$cumul_reglements = 0;
	$cumul_bases_produits = 0;
	$cumul_bases_services = 0;

	$rounding = min($conf->global->MAIN_MAX_DECIMALS_UNIT,$conf->global->MAIN_MAX_DECIMALS_TOT);
	if ($num > 0)
	{
		$var=true;
		$productBaseRupt = 0;
		$serviceBaseRupt = 0;
		$productCommissionRupt = 0;
		$serviceCommissionRupt = 0;
		$cumul_factureHTRupt = 0;
		$cumul_factureTTCRupt = 0;
		$cumul_paymentRupt = 0;

		$userid = null;
		$socid = null;
		$facid = null;
		$lineid = null;
        $payment_id = null;
        $userCommissionRates = array();
			$gotSomeCommission =0; //var pour checker si un client a de la commission

		$socurl = null;
		$i = 0;
		//$fromPhp7 = (version_compare(PHP_VERSION, '7.0.0') >= 0); // Used for usort() @see => https://stackoverflow.com/questions/44542089/usort-difference-php7-1-vs-php5-6
		
		while ($i < $num)
		{
			$objp = $db->fetch_object($result);
			
			// option: ne pas commissionner le commercial si un type de contact est commissionné sur cette facture
			if (!empty($conf->global->COMMISSION_RESTRICT_TO_CONTACT) && !empty($objp->facid) && ($objp->contact != $objp->agentid))
			{
				$sql = $select;
				$sql.= " AND f.rowid = ".$objp->facid." AND e.fk_c_type_contact is not null AND e.fk_socpeople <> ".$objp->agentid;
				$sql.= $groupby;
				$test = $db->query($sql);//echo '<br><br>'.$sql;
				if ($test)
				{
					if ($db->num_rows($test) > 0)
						continue;
				}
			}
			if ($i > 0)
			{
				
				
			    if ($agentid < 0 && $objp->agentid != $userid)
			    {			
					if($productCommissionRate+$serviceCommissionRate != 0){
						$productCommission = (! empty($productBase)?($productCommissionRate * $productBase / 100):0);
						$serviceCommission = (! empty($serviceBase)?($productCommissionRate * $serviceBase / 100):0);
						$productBaseRupt += $productBase;
						$serviceBaseRupt += $serviceBase;

						$cumul_factureHTRupt += $total;
						$cumul_factureTTCRupt += $total_ttc;
						$cumul_paymentRupt += $payment;
						$productCommissionRupt += $productCommission;
						$serviceCommissionRupt += $serviceCommission;

						$cumul_commissions_produit += $productCommission;
						$cumul_commissions_service += $serviceCommission;
						$cumul_factures_ht += $total;
						
						$cumul_factures_ttc += $total_ttc;
						$cumul_reglements += $payment;
						if($detail!=1 ||!empty($productCommissionRate))$cumul_bases_produits += $productBase;
						if($detail!=1 ||!empty($serviceCommissionRate))$cumul_bases_services += $serviceBase;
					}
					
 					displayCommissionLine($var, $socurl, null, null, null, $cumul_factureHTRupt, $cumul_factureTTCRupt,null, $cumul_paymentRupt, $userid, $productBaseRupt, null, $productCommissionRupt, $serviceBaseRupt, null, $serviceCommissionRupt,$productid,$label_categorie_societe);
					$productBaseRupt = 0;
					$serviceBaseRupt = 0;
					$productCommissionRupt = 0;
					$serviceCommissionRupt = 0;
					$cumul_factureHTRupt = 0;
					$cumul_factureTTCRupt = 0;
					$cumul_paymentRupt = 0;
			        $userid = $objp->agentid;
			        $socid = $objp->socid;
					// memorize invoice details
					$facid = $objp->facid;
					$lineid = $objp->facid;
					$ref = $objp->ref;
					$datef = $objp->datef;
					$total = $objp->total;
					$total_ttc = $objp->total_ttc;
					// memorize payment details
	                $payment_id = $objp->payment_id;
					$datep = $objp->datep;
					$payment = $objp->payment;
					$productid = $objp->productid;
					$label_categorie_societe=$objp->label_categorie_societe;
					// commission bases
					$productBase=(! empty($objp->productBase)?$objp->productBase:0);
					$serviceBase=(! empty($objp->serviceBase)?$objp->serviceBase:0);
	                $productCommissionRate = 0;
	                $serviceCommissionRate = 0;
					$userCommissionRates = array();
				}
				elseif ($agentid > 0 && $objp->socid != $socid)
			    {
					
					$productCommission = (! empty($productBase)?($productCommissionRate * $productBase / 100):0);
					$serviceCommission = (! empty($serviceBase)?($serviceCommissionRate * $serviceBase / 100):0);
				
if(!empty($gotSomeCommission)){
					$cumul_commissions_produit += $productCommission;
					$cumul_commissions_service += $serviceCommission;
					$cumul_factures_ht += $total;
					
					$cumul_factures_ttc += $total_ttc;
					$cumul_reglements += $payment;
					if($detail!=1 ||!empty($productCommissionRate))$cumul_bases_produits += $productBase;
					if($detail!=1 ||!empty($serviceCommissionRate))$cumul_bases_services += $serviceBase;

					if (!empty($detail))    // affichage ligne detail
					{
						
					    displayCommissionLine($var, $socurl, $facid, $ref, $datef, $total, $total_ttc, $datep, $payment, $objp->agentid, $productBase, $productCommissionRate, $productCommission, $serviceBase, $serviceCommissionRate, $serviceCommission,$productid,$label_categorie_societe);
						$gotSomeCommission=0;
						// $socid = $objp->socid;
						
					}
					else          // cumul rupture
					{
						$productBaseRupt += $productBase;
						$serviceBaseRupt += $serviceBase;
						
						$cumul_factureHTRupt += $total;
						$cumul_factureTTCRupt += $total_ttc;
						$cumul_paymentRupt += $payment;
						$productCommissionRupt += $productCommission;
						$serviceCommissionRupt += $serviceCommission;
						
						displayCommissionLine($var, $socurl, null, null, null, $cumul_factureHTRupt, $cumul_factureTTCRupt, null, $cumul_paymentRupt, $objp->agentid, $productBaseRupt, null, $productCommissionRupt, $serviceBaseRupt, null, $serviceCommissionRupt,$productid,$label_categorie_societe);
						$productBaseRupt = 0;
						$serviceBaseRupt = 0;
						$productCommissionRupt = 0;
						$serviceCommissionRupt = 0;
						$cumul_factureHTRupt = 0;
						$cumul_factureTTCRupt = 0;
						$cumul_paymentRupt = 0;
						$gotSomeCommission = 0;
						
					}
					
					
}					
			       $socid = $objp->socid;
					$companystatic->id=$objp->socid;
					$companystatic->nom=$objp->nom;
					$companystatic->client=$objp->client;
					$socurl = $companystatic->getNomUrl(1,'customer');
					
					// memorize invoice details
	                $facid = $objp->facid;
	                $lineid = $objp->lineid;
					$ref = $objp->ref;
					$datef = $objp->datef;
					$total = $objp->total;
					$total_ttc = $objp->total_ttc;
					// memorize payment details
	                $payment_id = $objp->payment_id;
					$datep = $objp->datep;
					$productid = $objp->productid;
					$label_categorie_societe = $objp->label_categorie_societe;
					$payment = $objp->payment;
					// commission bases
					$productBase=(! empty($objp->productBase)?$objp->productBase:0);
					$serviceBase=(! empty($objp->serviceBase)?$objp->serviceBase:0);
	                $productCommissionRate = 0;
	                $serviceCommissionRate = 0;
					$userCommissionRates = array();
				}
				// cumuler taux commissions et affichage ou cumul à rupture sur facture
				elseif ($facid != $objp->facid || $lineid != $objp->lineid)
				{
					
					$productCommission = (! empty($productBase)?($productCommissionRate * $productBase / 100):0);
					$serviceCommission = (! empty($serviceBase)?($serviceCommissionRate * $serviceBase / 100):0);
if(!empty($gotSomeCommission)){
	
					if (!empty($detail))    // affichage ligne detail
					{
						
					    displayCommissionLine($var, $socurl, $facid, $ref, $datef, $total, $total_ttc, $datep, $payment, $objp->agentid, $productBase, $productCommissionRate, $productCommission, $serviceBase, $serviceCommissionRate, $serviceCommission,$productid,$label_categorie_societe);
					//	$gotSomeCommission=0;
						
						
					}
					else if($facid != $objp->facid)      // cumul rupture
					{
						$productBaseRupt += $productBase;
						$serviceBaseRupt += $serviceBase;
						$cumul_factureHTRupt += $total;
						$cumul_factureTTCRupt += $total_ttc;
						$cumul_paymentRupt += $payment;
						$productCommissionRupt += $productCommission;
						$serviceCommissionRupt += $serviceCommission;
						$cumul_factures_ht += $total;
						$cumul_factures_ttc += $total_ttc;
						$cumul_reglements += $payment;
						$gotSomeCommission=0;
						
					}else{
						
						$productBaseRupt += $productBase;
						$serviceBaseRupt += $serviceBase;
						$productCommissionRupt += $productCommission;
						$serviceCommissionRupt += $serviceCommission;
					}
					
					$cumul_commissions_produit += $productCommission;
					$cumul_commissions_service += $serviceCommission;
					
					if($detail!=1 || !empty($productCommissionRate) )$cumul_bases_produits += $productBase;
					if(!empty($serviceCommissionRate) || $detail!=1)$cumul_bases_services += $serviceBase;
				
}
					// memorize invoice details
	                $facid = $objp->facid;
	                $lineid = $objp->lineid;
					$ref = $objp->ref;
					$datef = $objp->datef;
					$total = $objp->total;
					$productid = $objp->productid;
					$label_categorie_societe=$objp->label_categorie_societe;
					$total_ttc = $objp->total_ttc;
					// memorize payment details
	                $payment_id = $objp->payment_id;
					$datep = $objp->datep;
					$payment = $objp->payment;
					// commission bases
					$productBase=(! empty($objp->productBase)?$objp->productBase:0);
					$serviceBase=(! empty($objp->serviceBase)?$objp->serviceBase:0);
	                $productCommissionRate = 0;
	                $serviceCommissionRate = 0;
					$userCommissionRates = array();
					
				}
			// cumuler taux commissions et affichage ou cumul à rupture sur facture
				elseif ($payment_id != $objp->payment_id)
				{
					if($productCommissionRate+$serviceCommissionRate != 0){
					$productCommission = (! empty($productBase)?($productCommissionRate * $productBase / 100):0);
					$serviceCommission = (! empty($serviceBase)?($serviceCommissionRate * $serviceBase / 100):0);

					if (!empty($detail))    // affichage ligne detail
					{
					    displayCommissionLine($var, $socurl, $facid, $ref, $datef, $total, $total_ttc, $datep, $payment, $objp->agentid, $productBase, $productCommissionRate, $productCommission, $serviceBase, $serviceCommissionRate, $serviceCommission,$productid,$label_categorie_societe);
					}
					else          // cumul rupture
					{
						$productBaseRupt += $productBase;
						$serviceBaseRupt += $serviceBase;
						$cumul_factureHTRupt += $total;
						$cumul_factureTTCRupt += $total_ttc;
						$cumul_paymentRupt += $payment;
						$productCommissionRupt += $productCommission;
						$serviceCommissionRupt += $serviceCommission;
					}

					$cumul_commissions_produit += $productCommission;
					$cumul_commissions_service += $serviceCommission;
					
					$cumul_factures_ht += $total;
					$cumul_factures_ttc += $total_ttc;
					$cumul_reglements += $payment;
					if($detail!=1 ||!empty($productCommissionRate))$cumul_bases_produits += $productBase;
					if($detail!=1 ||!empty($serviceCommissionRate))$cumul_bases_services += $serviceBase;
					}
					// memorize payment details
	                $payment_id = $objp->payment_id;
					$datep = $objp->datep;
					$payment = $objp->payment;
					$productid = $objp->productid;
					$label_categorie_societe=$objp->label_categorie_societe;
					// commission bases
					$productBase=(! empty($objp->productBase)?$objp->productBase:0);
					$serviceBase=(! empty($objp->serviceBase)?$objp->serviceBase:0);
	                $productCommissionRate = 0;
	                $serviceCommissionRate = 0;
					$userCommissionRates = array();
					
				}
			}
			// memo infos user/societe/facture sur premiere boucle
   			else
			{
				$companystatic->id=$objp->socid;
				$companystatic->nom=$objp->nom;
				$companystatic->client=$objp->client;
				$socurl = $companystatic->getNomUrl(1,'customer');
				
				$socid = $objp->socid;
				$userid = $objp->agentid;
				// memorize invoice details
                $facid = $objp->facid;
                $lineid = $objp->lineid;
				$ref = $objp->ref;
				$datef = $objp->datef;
				$total = $objp->total;
				$total_ttc = $objp->total_ttc;
				// memorize payment details
                $payment_id = $objp->payment_id;
				$datep = $objp->datep;
				$productid = $objp->productid;
				$label_categorie_societe=$objp->label_categorie_societe;
				$payment = $objp->payment;
				// commission bases
				$productBase=(! empty($objp->productBase)?$objp->productBase:0);
				$serviceBase=(! empty($objp->serviceBase)?$objp->serviceBase:0);
                $productCommissionRate = 0;
                $serviceCommissionRate = 0;
				$userCommissionRates = array();
				
			}

			$i++;

			// recherche taux de commission
            if ( sizeof($userCommissionRates) == 0)
			{
			
				$productCommissionRate += getCommissionRate($objp->commercial, $objp->contact, $objp->fk_contact_type, $objp->agentid, $objp->socid, 0, $objp->total,$objp->categorie_product,$objp->categorie_societe);
				
				$serviceCommissionRate += getCommissionRate($objp->commercial, $objp->contact, $objp->fk_contact_type, $objp->agentid, $objp->socid, 1, $objp->total,$objp->categorie_product,$objp->categorie_societe);
				$gotSomeCommission +=$productCommissionRate+$serviceCommissionRate;
				
			}
		}
		if (!empty($facid))
		{
			// affichage derniere ligne rupture
			if (empty($detail))
			{
				if($productCommissionRate+$serviceCommissionRate != 0){
				$productCommission = (! empty($productBase)?($productCommissionRate * $productBase / 100):0);
				$serviceCommission = (! empty($serviceBase)?($serviceCommissionRate * $serviceBase / 100):0);
				$productBaseRupt += $productBase;
				$serviceBaseRupt += $serviceBase;
				
				$cumul_factureHTRupt += $total;
				$cumul_factureTTCRupt += $total_ttc;
				$cumul_paymentRupt += $payment;
				$productCommissionRupt += $productCommission;
				$serviceCommissionRupt += $serviceCommission;
				
				$cumul_commissions_produit += $productCommission;
				$cumul_commissions_service += $serviceCommission;
				$cumul_factures_ht += $total;
				$cumul_factures_ttc += $total_ttc;
				$cumul_reglements += $payment;
				if($detail!=1 ||!empty($productCommissionRate))$cumul_bases_produits += $productBase;
				if($detail!=1 ||!empty($serviceCommissionRate))$cumul_bases_services += $serviceBase;
				
				}
				
				displayCommissionLine($var, $socurl, null, null, null, $cumul_factureHTRupt, $cumul_factureTTCRupt, null, $cumul_paymentRupt, $objp->agentid, $productBaseRupt, null, $productCommissionRupt, $serviceBaseRupt, null, $serviceCommissionRupt,$productid,$label_categorie_societe);
			}
			else
			{
				$productCommission = (! empty($productBase)?($productCommissionRate * $productBase / 100):0);
				$serviceCommission = (! empty($serviceBase)?($serviceCommissionRate * $serviceBase / 100):0);
if($productCommissionRate+$serviceCommissionRate != 0){
				$cumul_commissions_produit += $productCommission;
				$cumul_commissions_service += $serviceCommission;
				
				$cumul_factures_ht += $total;
				$cumul_factures_ttc += $total_ttc;
				$cumul_reglements += $payment;
				if($detail!=1 ||!empty($productCommissionRate))$cumul_bases_produits += $productBase;
				if($detail!=1 ||!empty($serviceCommissionRate))$cumul_bases_services += $serviceBase;
				
}
				displayCommissionLine($var, $socurl, $facid, $ref, $datef, $total, $total_ttc, $datep, $payment, $objp->agentid, $productBase, $productCommissionRate, $productCommission, $serviceBase, $serviceCommissionRate, $serviceCommission,$productid,$label_categorie_societe);
			}
		}
	}

	// affichage totaux commission
	$var=!$var;
	print '<tr '.$bc[$var].' style="border-top: 1px solid #ccc; font-weight: bold">';
	if(!empty($arrayfields['customer']['checked']) && $agentid > 0 ||  !empty($arrayfields['commercialagent']['checked']) && $agentid <= 0){
		print '<td>';
		print $langs->trans('Total');
		print "</td>";
	}
	if(!empty($arrayfields['label_categorie_societe']['checked']) && $agentid > 0){
		print ' </td><td>';
	}
	if ($agentid > 0 && !empty($detail)){
		if(!empty($arrayfields['invoiceref']['checked']))print ' </td><td>';
		if(!empty($arrayfields['product']['checked']))print ' </td><td>';
		if(!empty($arrayfields['invoicedate']['checked']))print ' </td><td>';
	}
			
	
	
	// total facturé ht
	if(!empty($arrayfields['invoiceamountht']['checked']))print "<td align=\"right\" nowrap>".price($cumul_factures_ht, null, null, null, null, $rounding)."</td>\n";
	if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
	{
		if(!empty($arrayfields['invoiceamountttc']['checked']))print "<td align=\"right\" nowrap>".price($cumul_factures_ttc, null, null, null, null, $rounding)."</td>\n";
		if (!empty($detail))
				if(!empty($arrayfields['paymentdate']['checked']))print "<td align=\"center\" ></td>\n";
		if(!empty($arrayfields['paymentamount']['checked']))print "<td align=\"right\" nowrap>".price($cumul_reglements, null, null, null, null, $rounding)."</td>\n";
	}
	// product commission
	if(!empty($arrayfields['productbase']['checked']))print "<td align=\"right\" nowrap>".price($cumul_bases_produits, null, null, null, null, $rounding)."</td>\n";
	if (!empty($detail))
		if(!empty($arrayfields['productrate']['checked']))print "<td align=\"right\"></td>\n";
	if(!empty($arrayfields['productcommission']['checked']))print "<td align=\"right\" nowrap>".price($cumul_commissions_produit, null, null, null, null, $rounding)."</td>\n";
	// service commission
	if(!empty($arrayfields['servicebase']['checked']))print "<td align=\"right\" nowrap>".price($cumul_bases_services, null, null, null, null, $rounding)."</td>\n";
	if (!empty($detail))
		if(!empty($arrayfields['servicerate']['checked']))print "<td align=\"right\"></td>\n";
	if(!empty($arrayfields['servicecommission']['checked']))print "<td align=\"right\" nowrap>".price($cumul_commissions_service, null, null, null, null, $rounding)."</td>\n";
	// total commission
	if(!empty($arrayfields['totalcommission']['checked']))print "<td align=\"right\" nowrap>".price($cumul_commissions_produit + $cumul_commissions_service, null, null, null, null, $rounding)."</td>\n";
	print '<td></td>';
	print "</tr>\n";

	print "</td>";
	print "</table>";
	
}
else
{
	dol_print_error($db);
}
$db->free($result);
print "</form>";
llxFooter();
$db->close();

function cmp($taba, $tabb)
{
	$a = array_count_values($taba);
	$b = array_count_values($tabb);
	if ((int) $a[-1] <  (int) $b[-1]) return 1;
	elseif ((int) $a[-1] >  (int) $b[-1]) return -1;
	else return 0;
}

function getCommissionRate($commercial, $contact, $fk_contact_type, $agent, $fk_soc, $fk_product_type, $amount, $fk_catprod, $fk_catcust)
{
	global $conf, $db, $commissionRules, $userCommissionRates;
	
	$commissionRate = 0;

	if ($agent == $contact)
		$fk_user = $contact;
	else if($agent == $commercial)
		$fk_user = $commercial;
	else
		$fk_user = $agent;
	if(empty($contact))$contact=0;
	if(empty($commercial))$commercial=0;
	if(empty($fk_contact_type))$fk_contact_type=-20;
	
	//echo '<br>commercial: '.$commercial.' , contact: '.$contact.' , type de contact: '.$fk_contact_type.' , user: '.$fk_user.' , client: '.$fk_soc.' , type prod: '.$fk_product_type.'<br>';
	
	// liste des n-uplets à prendre en compte (dans l'ordre)
	$liste = array();
	$listeAgentCommercial=array();
	$catprod = explode(',', $fk_catprod);
	foreach ($catprod as $cprod)//Pour chaque categorie produit
	{
		$catcust = explode(',', $fk_catcust);
		foreach ($catcust as $ccust)//Pour chaque categorie cliente
		{
			$liste=array_merge($liste, array( //Si -1 ==> Tout
			
				array($contact, -1, $fk_contact_type, $cprod, -1, $fk_soc, $fk_product_type), // commission tous/type de contact/ categorie produit/ categ cliente/client/type de produit
				array($contact, -1, $fk_contact_type, -1, -1, $fk_soc, $fk_product_type), // commission tous/type de contact/ categorie produit/ categ cliente/client/type de produit
				array($contact, -1, $fk_contact_type, $cprod, -1, $fk_soc, -1), // commission tous/type de contact/ categorie produit/ categ cliente/client, tout type de produit
				array($contact, -1, $fk_contact_type, -1, -1, $fk_soc, -1), // commission tous/type de contact/ categorie produit/ categ cliente/client, tout type de produit
				array($contact, -1, $fk_contact_type, $cprod, $ccust, -1, $fk_product_type), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array($contact, -1, $fk_contact_type, $cprod, -1, -1, $fk_product_type), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array($contact, -1, $fk_contact_type, -1, $ccust, -1, $fk_product_type), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array($contact, -1, $fk_contact_type, -1, -1, -1, $fk_product_type), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array($contact, -1, $fk_contact_type, $cprod, $ccust, -1, -1), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($contact, -1, $fk_contact_type, $cprod, -1, -1, -1), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($contact, -1, $fk_contact_type, -1, $ccust, -1, -1), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($contact, -1, $fk_contact_type, -1, -1, -1, -1), // commission tous/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($commercial, -1, 0, $cprod, -1, $fk_soc, $fk_product_type), // commission tous/commercial/ categorie produit/ categ cliente/client/type de produit
				array($commercial, -1, 0, -1, -1, $fk_soc, $fk_product_type), // commission tous/commercial/ categorie produit/ categ cliente/client/type de produit
				array($commercial, -1, 0, $cprod, -1, $fk_soc, -1), // commission tous/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, -1, 0, -1, -1, $fk_soc, -1), // commission tous/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, -1, 0, $cprod, $ccust, -1, $fk_product_type), // commission tous/commercial/ categorie produit/ categ cliente/tout client/type de produit
				array($commercial, -1, 0, $cprod, -1, -1, $fk_product_type), // commission tous/commercial/ categorie produit/ categ cliente/tout client/type de produit
				array($commercial, -1, 0, -1, $ccust, -1, $fk_product_type), // commission tous/commercial/ categorie produit/ categ cliente/tout client/type de produit
				array($commercial, -1, 0, -1, -1, -1, $fk_product_type), // commission tous/commercial/ categorie produit/ categ cliente/tout client/type de produit
				array($commercial, -1, 0, $cprod, $ccust, -1, -1), // commission tous/commercial/ categorie produit/ categ cliente/tout client/tout type de produit
				array($commercial, -1, 0, $cprod, -1, -1, -1), // commission tous/commercial/ categorie produit/ categ cliente/tout client/tout type de produit
				array($commercial, -1, 0, -1, $ccust, -1, -1), // commission tous/commercial/ categorie produit/ categ cliente/tout client/tout type de produit
				array($commercial, -1, 0, -1, -1, -1, -1), // commission tous/commercial/ categorie produit/ categ cliente/tout client/tout type de produit
				array(-2, -1, -1, $cprod, -1, $fk_soc, $fk_product_type), // commission tous/tout type de contact/ categorie produit/ categ cliente/client/type de produit
				array(-2, -1, -1, -1, -1, $fk_soc, $fk_product_type), // commission tous/tout type de contact/ categorie produit/ categ cliente/client/type de produit
				array(-2, -1, -1, $cprod, -1, $fk_soc, -1), // commission tous/tout type de contact/ categorie produit/ categ cliente/client/tout type de produit
				array(-2, -1, -1, -1, -1, $fk_soc, -1), // commission tous/tout type de contact/ categorie produit/ categ cliente/client/tout type de produit
				array(-2, -1, -1, $cprod, $ccust, -1, $fk_product_type), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, -1, -1, $cprod, -1, -1, $fk_product_type), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, -1, -1, -1, $ccust, -1, $fk_product_type), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, -1, -1, -1, -1, -1, $fk_product_type), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, -1, -1, $cprod, $ccust, -1, -1), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout type de produit
				array(-2, -1, -1, $cprod, -1, -1, -1), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout type de produit
				array(-2, -1, -1, -1, $ccust, -1, -1), // commission tous/tout type de contact/ categorie produit/ categ cliente/tout type de produit
				array(-2, -1, -1, -1, -1, -1, -1) // commission tous/tout type de contact/ categorie produit/ categ cliente/tout type de produit
			));
			
			$listeAgentCommercial=array_merge($listeAgentCommercial,array(
				array($contact, $fk_user, $fk_contact_type, $cprod, -1, $fk_soc, $fk_product_type), // commission utilisateur/type de contact/categorie produit/toute categ cliente/client/type de produit
				array($contact, $fk_user, $fk_contact_type, -1, -1, $fk_soc, $fk_product_type), // commission utilisateur/type de contact/toute categorie produit/toute categ cliente/client/type de produit
				array($contact, $fk_user, $fk_contact_type, $cprod, -1, $fk_soc, -1), // commission utilisateur/type de contact/toute categorie produit/toute categ cliente/client/tout type de produit
				array($contact, $fk_user, $fk_contact_type, -1, -1, $fk_soc, -1), // commission utilisateur/type de contact/categorie produit/toute categ cliente/client/tout type de produit
				array($contact, $fk_user, $fk_contact_type, $cprod, $ccust, -1, $fk_product_type), // commission utilisateur/type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array($contact, $fk_user, $fk_contact_type, $cprod, -1, -1, $fk_product_type), // commission utilisateur/type de contact/ categorie produit/toute categ cliente/tout client/type de produit
				array($contact, $fk_user, $fk_contact_type, -1, $ccust, -1, $fk_product_type), // commission utilisateur/type de contact/ toute categorie produit/ categ cliente/tout client/type de produit
				array($contact, $fk_user, $fk_contact_type, -1, -1, -1, $fk_product_type), // commission utilisateur/type de contact/ toute categorie produit/toute categ cliente/tout client/type de produit
				array($contact, $fk_user, $fk_contact_type, $cprod, $ccust, -1, -1), // commission utilisateur/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($contact, $fk_user, $fk_contact_type, $cprod, -1, -1, -1), // commission utilisateur/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($contact, $fk_user, $fk_contact_type, -1, $ccust, -1, -1), // commission utilisateur/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($contact, $fk_user, $fk_contact_type, -1, -1, -1, -1), // commission utilisateur/type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array($commercial, $fk_user, 0, $cprod, -1, $fk_soc, $fk_product_type), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/type de produit
				array($commercial, $fk_user, 0, -1, -1, $fk_soc, $fk_product_type), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/type de produit
				array($commercial, $fk_user, 0, $cprod, -1, $fk_soc, -1), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, $fk_user, 0, -1, -1, $fk_soc, -1), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, $fk_user, 0, $cprod, $ccust, -1, $fk_product_type), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, $fk_user, 0, $cprod, -1, -1, $fk_product_type), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, $fk_user, 0, -1, $ccust, -1, $fk_product_type), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, $fk_user, 0, -1, -1, -1, $fk_product_type), // commission utilisateur/commercial/ categorie produit/ categ cliente/client/tout type de produit
				array($commercial, $fk_user, 0, $cprod, $ccust, -1, -1), // commission utilisateur/commercial/ categorie produit/ categ cliente/tout client, tout type de produit
				array($commercial, $fk_user, 0, $cprod, -1, -1, -1), // commission utilisateur/commercial/ categorie produit/ categ cliente/tout client, tout type de produit
				array($commercial, $fk_user, 0, -1, $ccust, -1, -1), // commission utilisateur/commercial/ categorie produit/ categ cliente/tout client, tout type de produit
				array($commercial, $fk_user, 0, -1, -1, -1, -1), // commission utilisateur/commercial/ categorie produit/ categ cliente/tout client, tout type de produit
				array(-2, $fk_user, -1, $cprod, -1, $fk_soc, $fk_product_type), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/client/type de produit
				array(-2, $fk_user, -1, -1, -1, $fk_soc, $fk_product_type), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/client/type de produit
				array(-2, $fk_user, -1, $cprod, -1, $fk_soc, -1), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/client/tout type de produit
				array(-2, $fk_user, -1, -1, -1, $fk_soc, -1), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/client/tout type de produit
				array(-2, $fk_user, -1, $cprod, $ccust, -1, $fk_product_type), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, $fk_user, -1, $cprod, -1, -1, $fk_product_type), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, $fk_user, -1, -1, $ccust, -1, $fk_product_type), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, $fk_user, -1, $cprod, -1, -1, $fk_product_type), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/type de produit
				array(-2, $fk_user, -1, $cprod, $ccust, -1, -1), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array(-2, $fk_user, -1, $cprod, -1, -1, -1), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array(-2, $fk_user, -1, -1, $ccust, -1, -1), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
				array(-2, $fk_user, -1, -1, -1, -1, -1), // commission utilisateur/tout type de contact/ categorie produit/ categ cliente/tout client/tout type de produit
			));
			if (empty($conf->global->COMMISSION_CUMUL))$liste=array_merge($liste,$listeAgentCommercial);
		//	usort($liste,"cmp");
		}
		//usort($liste,"cmp");
	}
	
	//print_r($liste);
	usort($liste,"cmp");
	
	if (!empty($conf->global->COMMISSION_CUMUL))usort($listeAgentCommercial,"cmp");

	// parcours de la liste des règles et calcul taux commission
	foreach($liste as $regle)
	{
		
		if (($regle[0] == -2|| $regle[0] == $fk_user) && $commissionRules[$regle[1]] && $commissionRules[$regle[1]][$regle[2]] && $commissionRules[$regle[1]][$regle[2]][$regle[3]]
			&& $commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]]
			&& $commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]]&& $commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]][$regle[6]])
		{
			
		
	
	        if (!empty($conf->global->COMMISSION_RESTRICT_TO_CONTACT) && $commercial == $contact && $commissionRate != 0)
	            return $commissionRate;
	
			$applied_rate = 0;
			foreach($commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]][$regle[6]] as $limite => $rate)
			{
				if ($limite == 0)
					$applied_rate = $rate;
			    elseif ($amount < $limite)
				{	$applied_rate = $rate;
					break;
				}
			}
	
			if ($applied_rate != 0)
			{
	            $userCommissionRates[$fk_product_type][$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]][$regle[6]] = $applied_rate;
		       
				$commissionRate =  $applied_rate;
			
				
			}
		}
	}
	
	if (!empty($conf->global->COMMISSION_CUMUL))
	{
		foreach ($listeAgentCommercial as $regle)
		{

			if (($regle[0] == -2 || $regle[0] == $fk_user) && $commissionRules[$regle[1]] && $commissionRules[$regle[1]][$regle[2]] && $commissionRules[$regle[1]][$regle[2]][$regle[3]] && $commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]] && $commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]] && $commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]][$regle[6]])
			{



				if (!empty($conf->global->COMMISSION_RESTRICT_TO_CONTACT) && $commercial == $contact && $commissionRate != 0)
					return $commissionRate;

				$applied_rate = 0;
				foreach ($commissionRules[$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]][$regle[6]] as $limite => $rate)
				{
					if ($limite == 0)
						$applied_rate = $rate;
					elseif ($amount < $limite)
					{
						$applied_rate = $rate;
						break;
					}
				}

				if ($applied_rate != 0)
				{
					$userCommissionRates[$fk_product_type][$regle[1]][$regle[2]][$regle[3]][$regle[4]][$regle[5]][$regle[6]] = $applied_rate;
					
					$commissionRate += $applied_rate;
					break;
				}
			}
		}
	}
	
	

	return $commissionRate;
}



function getCommissionRules()
{
	global $conf, $db;

	$commissionRules = array();

	$sql = "select fk_c_type_contact as fk_contact_type, fk_user, fk_prodcat, fk_custcat, fk_soc, fk_product_type, rate, limite";
	$sql .= " from ".MAIN_DB_PREFIX."commissions_rules";
	$sql .= " ORDER BY fk_c_type_contact, fk_user, fk_soc, fk_product_type, limite, fk_prodcat, fk_custcat";
	$resql = $db->query($sql);
	if ($resql)
	{
		$nbrows = $db->num_rows($resql);
		if ($nbrows > 0)
		{
			$i = 0;
			while ($i < $nbrows)
			{
				$obj = $db->fetch_object($resql);

				//Tri
				$fk_prodcat = explode(',', $obj->fk_prodcat);
				foreach ($fk_prodcat as $prodcast)
				{

					$fk_custcat = explode(',', $obj->fk_custcat);
					foreach ($fk_custcat as $custcat)
					{
//				sort($obj->fk_custcat);
//				$obj->fk_custcat=implode(',',$obj->fk_custcat);
						//Tri
//				sort($obj->fk_prodcat);
//				$obj->fk_prodcat=implode(',',$obj->fk_prodcat);
//			
						$commissionRules[$obj->fk_user][$obj->fk_contact_type][$prodcast][$custcat][$obj->fk_soc][$obj->fk_product_type][$obj->limite] = $obj->rate;
					}
				}
				$i++;
				
			}
		}
		
	}
	else
	{
		dol_print_error($db);
		return -1;
	}

	return $commissionRules;
}

function getUserNotLinked(){
	global $commissionRules;
	$tab = array();
	foreach ($commissionRules as $key => $value){
		$tab[] = $key;
	}
	return $tab;
}

function displayCommissionLine($var, $socurl, $facid, $ref, $datef, $amountHT, $amountTTC, $datep, $payment, $userid, $productBase, $productCommissionRate, $productCommission, $serviceBase, $serviceCommissionRate, $serviceCommission, $productid = null,$label_categorie_societe="")
{
	global $db, $detail, $bc, $var, $conf, $langs, $rounding, $docstatic, $userstatic, $agentid, $arrayfields;
	if($productCommission+ $serviceCommission != 0){
	$var=!$var; 
	print "<tr $bc[$var]>";
	if ($agentid > 0) {
		if(!empty($arrayfields['customer']['checked']))print "<td>".$socurl."</td>\n";
		if(!empty($arrayfields['label_categorie_societe']['checked']))print "<td>".$label_categorie_societe."</td>\n";

		if (!empty($detail))
		{	
			$product = new Product($db);
			if(!empty($productid)){
				$product->fetch($productid);
				$proddesc = $product->getNomUrl(1,'',200,0);
			} else {
				$proddesc = 'Ligne libre';
			}
			
            $docstatic->id=$facid;
            $docstatic->ref=$ref;
			if(!empty($arrayfields['invoiceref']['checked']))print "<td>".$docstatic->getNomUrl(1,'',200,0)."</td>\n";
			if(!empty($arrayfields['product']['checked']))print "<td align=\"center\">".$proddesc."</td>\n";
		    if(!empty($arrayfields['invoicedate']['checked']))print "<td align=\"center\">".dol_print_date($db->jdate($datef),'day')."</td>\n";
		    if(!empty($arrayfields['invoiceamountht']['checked']))print "<td align=\"right\" nowrap>".price($amountHT)."</td>\n";
			if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
			{
		    	if(!empty($arrayfields['invoiceamountttc']['checked']))print "<td align=\"right\" nowrap>".price($amountTTC)."</td>\n";
			    if(!empty($arrayfields['paymentdate']['checked']))print "<td align=\"center\">".dol_print_date($db->jdate($datep),'day')."</td>\n";
			    if(!empty($arrayfields['paymentamount']['checked']))print "<td align=\"right\" nowrap>".price($payment)."</td>\n";
			}
		}
		else
		{
		    if(!empty($arrayfields['invoiceamountht']['checked']))print "<td align=\"right\" nowrap>".price($amountHT)."</td>\n";
			if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
			{
                if(!empty($arrayfields['invoiceamountttc']['checked']))print "<td align=\"right\" nowrap>".price($amountTTC)."</td>\n";
			    if(!empty($arrayfields['paymentamount']['checked']))print "<td align=\"right\" nowrap>".price($payment)."</td>\n";
			}
		}
	}
	else {
		$userstatic->fetch($userid);
		if(!empty($arrayfields['commercialagent']['checked']))print "<td>".$userstatic->getFullName($langs,0,0,0)."</td>\n";
	    if(!empty($arrayfields['invoiceamountht']['checked']))print "<td align=\"right\" nowrap>".price($amountHT)."</td>\n";
		if ($conf->global->COMMISSION_METHOD == "PAYMENT" || $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
		{
            if(!empty($arrayfields['invoiceamountttc']['checked']))print "<td align=\"right\" nowrap>".price($amountTTC)."</td>\n";
		    if(!empty($arrayfields['paymentamount']['checked']))print "<td align=\"right\" nowrap>".price($payment)."</td>\n";
		}
	}

	if(!empty($arrayfields['productbase']['checked']))print "<td align=\"right\" nowrap>".price($productBase, null, null, null, null, $rounding)."</td>\n";
	if (!empty($detail))
		if(!empty($arrayfields['productrate']['checked']))print "<td align=\"right\">".price($productCommissionRate, null, null, null, null, $rounding)."%</td>\n";
	if(!empty($arrayfields['productcommission']['checked']))print "<td align=\"right\" nowrap>".price($productCommission, null, null, null, null, $rounding)."</td>\n";

	if(!empty($arrayfields['servicebase']['checked']))print "<td align=\"right\" nowrap>".price($serviceBase, null, null, null, null, $rounding)."</td>\n";
	if (!empty($detail))
		if(!empty($arrayfields['servicerate']['checked']))print "<td align=\"right\">".price($serviceCommissionRate, null, null, null, null, $rounding)."%</td>\n";
	if(!empty($arrayfields['servicecommission']['checked']))print "<td align=\"right\" nowrap>".price($serviceCommission, null, null, null, null, $rounding)."</td>\n";

	// total commission
	if(!empty($arrayfields['totalcommission']['checked']))print "<td align=\"right\" nowrap >".price($productCommission + $serviceCommission, null, null, null, null, $rounding)."</td>\n";
	print '<td></td>';
	print "</tr>\n";
	}
}

?>
<style>
div.selMonth .ui-datepicker-calendar {
    display: none;
}
div.selMonth select.ui-datepicker-month {
	width: 100px;
}
div.selMonth select.ui-datepicker-year {
	width: 80px;
}
div.selMonth .ui-datepicker, div.selMonth .ui-datepicker-header {
	width: 240px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {

	var fullMonthNames = $.datepicker.regional['<?php echo $langs->defaultlang ?>'].monthNames;

	$("div.selMonth").datepicker({
	    <?php
			if (!empty($_POST['startdatemonth']))
			    print 'defaultDate: new Date('.$_POST['startdateyear'].','.$_POST['startdatemonth'].'-1,'.$_POST['startdateday'].'),';
		?>
	    monthNamesShort: fullMonthNames,
	    changeMonth: true,
	    changeYear: true,
	    dateFormat: 'MM yy',
	    onChangeMonthYear: function(year, month, inst) {
			var dt = new Date(year, month-1, 1);
			$('#startdate').val(formatDate(dt,'<?php echo $langs->trans("FormatDateShortJavaInput"); ?>')).change();
			dt = new Date(year, month, 0);
			$('#enddate').val(formatDate(dt,'<?php echo $langs->trans("FormatDateShortJavaInput"); ?>')).change();
		}
	});
	
});
</script>
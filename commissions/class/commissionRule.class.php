<?php
/* Copyright (C) 2013      Christophe Battarel  <christophe.battarel@altairis.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file       custom/commssions/class/commissionRule.class.php
 * 	\ingroup    commission
 * 	\brief      File of class to manage commission rules
 */

/**
 * 	\class      CommissionRule
 * 	\brief      Class to manage commission rules
 */
class CommissionRule
{
    var $db;
    var $error;

	var $rowid;

    /**
	 *	Constructor
	 *
	 *  @param		DoliDB		$db      Database handler
     */
    function __construct($db)
    {
        $this->db = $db;

    }


	function delete($id) {
		global $langs, $conf;
		
		$this->db->begin();

		// remove supplier price
        $sql = "DELETE FROM ".MAIN_DB_PREFIX."commissions_rules";
        $sql.= " WHERE rowid = ".$this->db->escape($id);

        dol_syslog(get_class($this)."::remove_commissions_rules sql=".$sql);
        $resql = $this->db->query($sql);
        if ($resql)
        {
			$this->db->commit();
			return 1;
        }
        else
        {
            $this->error=$this->db->lasterror();
            dol_syslog(get_class($this)."::remove_commissions_rules ".$this->error,LOG_ERR);
            $this->db->rollback();
            return -1;
		}
	}

	function fetch($id) {
		global $langs, $conf;

		$sql = "select rowid, fk_c_type_contact as fk_contact_type, fk_user, fk_soc,fk_prodcat,fk_custcat, fk_product_type, rate, limite";
		$sql .= " from ".MAIN_DB_PREFIX."commissions_rules where rowid = ".$id;
		dol_syslog(get_class($this)."::fetch sql=".$sql);
		$resql = $this->db->query($sql);
		if ( $resql )
		{
			if ($this->db->num_rows($resql) > 0)
			{
				$obj = $this->db->fetch_object($resql);


				$this->rowid = $obj->rowid;
				$this->fk_contact_type = $obj->fk_contact_type;
				$this->fk_user = $obj->fk_user;
				$this->fk_product_type = $obj->fk_product_type;
				$this->fk_soc = $obj->fk_soc;
				$this->fk_prodcat = $obj->fk_prodcat;
				$this->fk_custcat = $obj->fk_custcat;
				$this->rate = $obj->rate;
				$this->limite = $obj->limite;
				$this->db->free($resql);
				return 0;
			}
		}
		else
		{
			dol_print_error($this->db);
			return -1;
		}
	}

    function create()
    {
        global $langs,$conf;

        $now=dol_now();

        // Clean parameters
        if (empty($this->rate)) $this->rate=0;
        if (empty($this->limite)) $this->limite=0;
        if (empty($this->fk_prodcat)) $this->fk_prodcat=-1;
		else $this->fk_prodcat = implode(',',$this->fk_prodcat);
        if (empty($this->fk_custcat)) $this->fk_custcat=-1;
		else $this->fk_custcat = implode(',',$this->fk_custcat);
		$this->rate = price2num($this->rate);
		$this->limite = price2num($this->limite);

        dol_syslog(get_class($this)."::create");

        $this->db->begin();
		
		// delete eventual same rule
		$sql = "DELETE FROM ".MAIN_DB_PREFIX."commissions_rules";
		$sql.= " WHERE fk_c_type_contact = ".$this->db->escape($this->fk_contact_type);
		$sql.= " AND fk_user = ".$this->db->escape($this->fk_user);
		$sql.= " AND fk_product_type = ".$this->db->escape($this->fk_product_type);
		$sql.= " AND fk_soc = ".$this->db->escape($this->fk_soc);
		$sql.= " AND fk_prodcat = '".$this->db->escape($this->fk_prodcat);
		$sql.= "' AND fk_custcat = '".$this->db->escape($this->fk_custcat);
		$sql.= "' AND limite = ".$this->db->escape($this->limite);
		$sql.= " AND entity = ".$conf->entity;
		
        $resql = $this->db->query($sql);
        if ($resql)
        {
	        // Insert into database
	        $sql = "INSERT INTO ".MAIN_DB_PREFIX."commissions_rules (";
	        $sql.= "fk_c_type_contact";
	        $sql.= ", fk_user";
	        $sql.= ", fk_product_type";
	        $sql.= ", fk_soc";
	        $sql.= ", fk_prodcat";
	        $sql.= ", fk_custcat";
	        $sql.= ", rate";
	        $sql.= ", limite";
	        $sql.= ", entity";
	        $sql.= ", datec";
	        $sql.= ") ";
	        $sql.= " VALUES (";
	        $sql.= $this->db->escape($this->fk_contact_type);
	        $sql.= ", ".$this->db->escape($this->fk_user);
	        $sql.= ", ".$this->db->escape($this->fk_product_type);
	        $sql.= ", ".$this->db->escape($this->fk_soc);
	        $sql.= ", '".$this->db->escape($this->fk_prodcat);
	        $sql.= "', '".$this->db->escape($this->fk_custcat);
	        $sql.= "', ".$this->db->escape($this->rate);
	        $sql.= ", ".$this->db->escape($this->limite);
	        $sql.= ", ".$conf->entity;
	        $sql.= ", '".$this->db->idate($now)."'";
	        $sql.= ")";

	        dol_syslog(get_class($this)."::create sql=".$sql, LOG_DEBUG);
	        $resql=$this->db->query($sql);
	        if ($resql)
	        {
	            $this->id = $this->db->last_insert_id(MAIN_DB_PREFIX."commissions_rules");

	            if ($this->id)
	            {
	                $this->db->commit();
	                dol_syslog(get_class($this)."::create done id=".$this->id);
	                return $this->id;
	            }
	            else
	            {
	                $this->error=$this->db->error();
	                dol_syslog(get_class($this)."::create -2 ".$this->error, LOG_ERR);
	                $this->db->rollback();
	                return -2;
	            }
	        }
	        else
	        {
	            $this->error=$this->db->error();
	            dol_syslog(get_class($this)."::create -1 ".$this->error, LOG_ERR);
	            $this->db->rollback();
	            return -1;
	        }
        }
        else
        {
            $this->error=$this->db->lasterror();
            dol_syslog(get_class($this)."::remove_commissions_rules ".$this->error,LOG_ERR);
            $this->db->rollback();
            return -1;
		}
    }
}

?>
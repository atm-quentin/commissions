<?php
/* Copyright (C) 2012	Christophe Battarel	    <christophe.battarel@altairis.fr>
 * Copyright (C) 2012	Laurent Destailleur		<eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   commissions     Module commissions
 * 	\brief      Module commissions
 * 	\file       htdocs/core/modules/modCommissions.class.php
 * 	\ingroup    commissions
 * 	\brief      Description and activation file for module Commissions
 */
include_once DOL_DOCUMENT_ROOT .'/core/modules/DolibarrModules.class.php';


/**
 * 	Class to describe modude Commisions
 */
class modCommissions extends DolibarrModules
{
    /**
     * 	Constructor
     *
     * 	@param	DoliDB	$db		Database handler
     */
	function __construct($db)
	{
		$this->db = $db;
		//creation vues
		$sql = "create view ".MAIN_DB_PREFIX."contact_commande as select e.element_id, e.fk_socpeople, e.fk_c_type_contact from ".MAIN_DB_PREFIX."element_contact e left join ".MAIN_DB_PREFIX."c_type_contact as tc ON tc.rowid = e.fk_c_type_contact where e.statut = 4 and tc.element='commande' AND tc.source='internal'";
		$this->db->query($sql);
		
		$sql = "create view ".MAIN_DB_PREFIX."contact_facture as select e.element_id, e.fk_socpeople, e.fk_c_type_contact from ".MAIN_DB_PREFIX."element_contact e left join ".MAIN_DB_PREFIX."c_type_contact as tc ON tc.rowid = e.fk_c_type_contact where e.statut = 4 and tc.element='facture' AND tc.source='internal'";
		$this->db->query($sql);
		
		// Id for module (must be unique).
		// Use here a free id (See in Home -> System information -> Dolibarr for list of used modules id).
		$this->numero = 140002;
		// Key text used to identify module (for permissions, menus, etc...)
		$this->rights_class = 'Commissions';

		// Family can be 'crm','financial','hr','projects','products','ecm','technic','other'
		// It is used to group modules in module setup page
		$this->family = "financial";
		// Module label (no space allowed), used if translation string 'ModuleXXXName' not found (where XXX is value of numeric property 'numero' of module)
		$this->name = preg_replace('/^mod/i','',get_class($this));
		// Module description, used if translation string 'ModuleXXXDesc' not found (where XXX is value of numeric property 'numero' of module)
		$this->description = "Commissions management";
		// Possible values for version are: 'development', 'experimental', 'dolibarr' or version
		$this->version = 'dolibarr';
		// Key used in llx_const table to save module status enabled/disabled (where MYMODULE is value of property name of module in uppercase)
		$this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);
		// Where to store the module in setup page (0=common,1=interface,2=other)
		$this->special = 2;
		// Name of png file (without png) used for this module.
		// Png file must be in theme/yourtheme/img directory under name object_pictovalue.png.
		$this->picto='commissions@commissions';

		// Data directories to create when module is enabled.
		$this->dirs = array();

		// Config pages. Put here list of php page names stored in admin directory used to setup module.
		$this->config_page_url = array('admin.php@commissions');

		// Dependencies
		$this->depends = array("modFacture");		// List of modules id that must be enabled if this module is enabled
		$this->requiredby = array();	// List of modules id to disable if this one is disabled
		$this->phpmin = array(5,1);					// Minimum version of PHP required by module
		$this->need_dolibarr_version = array(3,2);	// Minimum version of Dolibarr required by module
		$this->langfiles = array("commissions@commissions");

		// Constants
		$this->const = array(0=>array('COMMISSION_BASE',"chaine","TURNOVER",'Default commission base',0),
							 1=>array('COMMISSION_METHOD',"chaine","PAYMENT",'Default commission method',0),
							 2=>array('COMMISSION_CUMUL',"bool",false,'Default commission cumul',0),
							 3=>array('COMMISSION_RESTRICT_TO_CONTACT',"bool",false,'Default commission option',0)
							 );

		// hooks, triggers, css, ...
		$this->module_parts = array(
			'css'=>array('/commissions/css/commissions.css')
		);

		// New pages on tabs
		$this->tabs = array();

		// Boxes
		$this->boxes = array();			// List of boxes
		$r=0;

		// Main menu entries
		$this->menu = array();			// List of menus to add
		$r = 0;

		// left menu entry
		$this->menu[$r]=array(
				'fk_menu'=>'fk_mainmenu=accountancy',			// Put 0 if this is a top menu
    			'type'=>'left',			// This is a Top menu entry
    			'titre'=>'Commissions',
    			'mainmenu'=>'accountancy',
    			'leftmenu'=>'commissions',		// Use 1 if you also want to add left menu entries using this descriptor. Use 0 if left menu entries are defined in a file pre.inc.php (old school).
    			'url'=>'/commissions/index.php',
    			'langs'=>'commissions@commissions',	// Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
    			'position'=>200,
    			'enabled'=>'1',			// Define condition to show or hide menu entry. Use '$conf->monmodule->enabled' if entry must be visible if module is enabled.
    			'perms'=>'1',			// Use 'perms'=>'$user->rights->monmodule->level1->level2' if you want your menu with a permission rules
    			'target'=>'',
    			'user'=>0);				// 0=Menu for internal users, 1=external users, 2=both
		$r++;
		
		$this->menu[$r]=array(
				'fk_menu'=>'fk_mainmenu=accountancy,fk_leftmenu=commissions',			// Put 0 if this is a top menu
    			'type'=>'left',			// This is a Top menu entry
    			'titre'=>'CommissionRules',
    			'url'=>'/commissions/admin/rules.php',
    			'langs'=>'commissions@commissions',	// Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
    			'position'=>300,
    			'enabled'=>'1',			// Define condition to show or hide menu entry. Use '$conf->monmodule->enabled' if entry must be visible if module is enabled.
    			'perms'=>'$user->rights->commissions->rules',			// Use 'perms'=>'$user->rights->monmodule->level1->level2' if you want your menu with a permission rules
    			'target'=>'',
    			'user'=>0);				// 0=Menu for internal users, 1=external users, 2=both
		$r++;

		$this->menu[$r]=array(
				'fk_menu'=>'fk_mainmenu=accountancy,fk_leftmenu=commissions',			// Put 0 if this is a top menu
    			'type'=>'left',			// This is a Top menu entry
    			'titre'=>'CalculateCommissions',
     			'url'=>'/commissions/calculate.php',
    			'langs'=>'commissions@commissions',	// Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
    			'position'=>400,
    			'enabled'=>'1',			// Define condition to show or hide menu entry. Use '$conf->monmodule->enabled' if entry must be visible if module is enabled.
    			'perms'=>'$user->rights->commissions->lire || $user->rights->commissions->liretous',			// Use 'perms'=>'$user->rights->monmodule->level1->level2' if you want your menu with a permission rules
    			'target'=>'',
    			'user'=>0);				// 0=Menu for internal users, 1=external users, 2=both
		$r++;

		$this->menu[$r]=array(
				'fk_menu'=>'fk_mainmenu=accountancy,fk_leftmenu=commissions',			// Put 0 if this is a top menu
    			'type'=>'left',			// This is a Top menu entry
    			'titre'=>'Setup',
    			'url'=>'/commissions/admin/setup.php',
    			'langs'=>'commissions@commissions',	// Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
    			'position'=>500,
    			'enabled'=>'1',			// Define condition to show or hide menu entry. Use '$conf->monmodule->enabled' if entry must be visible if module is enabled.
    			'perms'=>'$user->rights->commissions->setup',			// Use 'perms'=>'$user->rights->monmodule->level1->level2' if you want your menu with a permission rules
    			'target'=>'',
    			'user'=>0);				// 0=Menu for internal users, 1=external users, 2=both
		$r++;

		// Permissions
		$this->rights = array();
		$this->rights_class = 'commissions';
		$r=0;

		$r++;
		$this->rights[$r][0] = 1400021; // id de la permission
		$this->rights[$r][1] = 'Consult any commercial agent commissions informations'; // libelle de la permission
		$this->rights[$r][3] = 0; // La permission est-elle une permission par defaut
		$this->rights[$r][4] = 'liretous';

		$r++;
		$this->rights[$r][0] = 1400022; // id de la permission
		$this->rights[$r][1] = 'Consult only its commission informations'; // libelle de la permission
		$this->rights[$r][3] = 0; // La permission est-elle une permission par defaut
		$this->rights[$r][4] = 'lire';

		$r++;
		$this->rights[$r][0] = 1400023; // id de la permission
		$this->rights[$r][1] = 'Define commission rules'; // libelle de la permission
		$this->rights[$r][3] = 0; // La permission est-elle une permission par defaut
		$this->rights[$r][4] = 'rules';

		$r++;
		$this->rights[$r][0] = 1400024; // id de la permission
		$this->rights[$r][1] = 'Setup commission module'; // libelle de la permission
		$this->rights[$r][3] = 0; // La permission est-elle une permission par defaut
		$this->rights[$r][4] = 'setup';

 	}

	/**
     *	Function called when module is enabled.
     *	The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
     *	It also creates data directories.
     *
	 *  @return     int             1 if OK, 0 if KO
     */
 	function init($sql = array(), $options='')
  	{
		$result=$this->load_tables();
			
		return $this->_init($sql,$options);
  	}

	/**
	 *	Function called when module is disabled.
 	 *  Remove from database constants, boxes and permissions from Dolibarr database.
 	 *	Data directories are not deleted.
 	 *
	 *  @return     int             1 if OK, 0 if KO
 	 */
  	function remove($options = '')
	{
    	$sql = array();

    	return $this->_remove($sql);
  	}


	/**
	 *	Create tables and keys required by module
	 * 	Files mymodule.sql and mymodule.key.sql with create table and create keys
	 * 	commands must be stored in directory /mymodule/sql/
	 *	This function is called by this->init.
	 *
	 * 	@return		int		<=0 if KO, >0 if OK
	 */
	function load_tables()
	{
		return $this->_load_tables('/commissions/sql/');
	}
}

?>

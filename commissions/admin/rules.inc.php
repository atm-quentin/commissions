<?php
/* Copyright (C) 2012	Christophe Battarel	<christophe.battarel@altairis.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       /htdocs/custom/commissions/admin/rates.php
 *		\ingroup    prices
 *		\brief      Page to configure commissions rates
 */

if( ! $user->rights->commissions->rules) accessforbidden();

dol_include_once('/commissions/class/commissionRule.class.php');

$langs->load("commissions@commissions");

/*
 * Action
 */
$id = GETPOST('id');
if (GETPOST('action') == 'delete' && !empty($id))
{
	$rule = new CommissionRule($db);
	if ($rule->delete($id))
        print '<div class="info">'.$langs->trans('LineRemoved').'</div>';
	else
        dol_print_error($db,$rule->error);
}

if (GETPOST('action') == 'validate')
{
	$i = 0;
	
	foreach($_REQUEST as $key => $value)
	{
		if(substr($key, 0, 5) == 'rate_' && !empty($value))
		{
			$rule = new CommissionRule($db);

			$rowid = substr($key, 5);

			// delete old line
			if (!empty($rowid))
				$rule->delete($rowid);
			
			// create new line
			$rule->fk_user = GETPOST('agentId_'.$rowid);
			$rule->fk_contact_type = GETPOST('contactType_'.$rowid);
			$rule->fk_soc = GETPOST('soc_'.$rowid);
			$rule->fk_prodcat = GETPOST('prodcats_'.$rowid);
			$rule->fk_custcat = GETPOST('custcats_'.$rowid);
			$rule->fk_product_type = GETPOST('productType_'.$rowid);
			$rule->rate = GETPOST('rate_'.$rowid);
			$rule->limite = GETPOST('limite_'.$rowid);
			$ret = $rule->create();
			if($ret <= 0)
			{
				dol_print_error($db,$rule->error);
			}
			else
				$i++;
		}
	}
	if ($i > 0)
		print '<div class="info">'.$langs->trans('LinesUpdated', $i).'</div>';
}
/*
 * View
 */

$form = new Form($db);

if ($conf->global->COMMISSION_METHOD == "ORDER")
{	// liste des types de contacts associés aux commandes
	require_once(DOL_DOCUMENT_ROOT.'/commande/class/commande.class.php');
	$commande = new Commande($db);
	$commande->element = 'commande';
	$contactTypes = $commande->liste_type_contact();
}
else
{	// liste des types de contacts associés aux factures
	require_once(DOL_DOCUMENT_ROOT.'/compta/facture/class/facture.class.php');
	$facture = new Facture($db);
	$facture->element = 'facture';
	$contactTypes = $facture->liste_type_contact();
}
print '<form method="post" action="'.$_SERVER['PHP_SELF'].'?tab=rules">';

print '<input type="hidden" name="action" value="validate" />';

print '<table id="rates" class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<th class="liste_titre">'.$langs->trans("CommercialAgent").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("ContactType").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("ProductCategory").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("CustomerCategory").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("Customer").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("ProductService").'</th>'."\n";
if ($conf->global->COMMISSION_METHOD == "ORDER")
	print '<th class="liste_titre">'.$langs->trans("OrdersLesserThan").'</th>'."\n";
else
	print '<th class="liste_titre">'.$langs->trans("InvoicesLesserThan").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("CommissionRate").'</th>'."\n";
print '<th class="liste_titre">'.$langs->trans("Remove").'</th>';//boutons
print '</tr>';

$var=true;

// liste des règles existantes
$sql = "SELECT rowid, fk_c_type_contact as fk_contact_type, fk_user, fk_soc,fk_prodcat,fk_custcat, fk_product_type, rate, limite";
$sql .= " FROM ".MAIN_DB_PREFIX."commissions_rules";
$sql .= " ORDER BY fk_c_type_contact, fk_user, fk_soc, fk_product_type, limite,fk_prodcat,fk_custcat";

$resql = $db->query($sql);
if ( $resql )
{
	$nbrows = $db->num_rows($resql);
	if ($nbrows > 0)
	{
		$i = 0;
		while ($i < $nbrows)
		{
			$obj = $db->fetch_object($resql);
			$var = !$var;
			print '<tr '.$bc[$var].'>';
			// user
			print '<td>';
			print $form->select_dolusers($obj->fk_user,'agentId_'.$obj->rowid,1);
			print '</td>';
			// contact type
			print '<td>';
			print '<select class="flat" name="contactType_'.$obj->rowid.'">';
			print '<option value="-1"';
				if ($obj->fk_contact_type == -1)
					print ' selected=selected ';
			print '>&nbsp;</option>';
			print '<option value="0"';
				if ($obj->fk_contact_type == 0)
					print ' selected=selected ';
			print '>'.$langs->trans('CustomerAgent').'</option>';
			foreach($contactTypes as $key=>$value)
			{
				print '<option value="'.$key.'"';
				if ($key == $obj->fk_contact_type)
					print ' selected=selected ';
				print '>'.$value.'</option>';
			}
			print "</select>\n";
			print '</td>';
			// product categ
			print '<td>';
			$obj->fk_prodcat = explode(',',$obj->fk_prodcat);
			$cate_arbo = $form->select_all_categories('product', null, 'parent', null, null, 1);
			print $form->multiselectarray('prodcats_'.$obj->rowid, $cate_arbo, $obj->fk_prodcat , null, null, null, null, "90%");
			print '</td>';
			// customer categ
			print '<td>';
			$obj->fk_custcat = explode(',',$obj->fk_custcat);
			$cate_arbo = $form->select_all_categories(Categorie::TYPE_CUSTOMER, null, null, null, null, 1);
			print $form->multiselectarray('custcats_'.$obj->rowid, $cate_arbo, $obj->fk_custcat, '', 0, '', 0, '90%');
			print '</td>';
			// customer
			print '<td>';
			print $form->select_company($obj->fk_soc,'soc_'.$obj->rowid,'s.client = 1', true, 0, 0, array(), 0, 'minwidth100 maxwidth200');
			print '</td>';
			// product type
			print '<td>';
			echo $form->select_type_of_lines($obj->fk_product_type,'productType_'.$obj->rowid,true,true);
			print '</td>';
			// commission limit
			print '<td>';
			print '<input class="right" type="text" name="limite_'.$obj->rowid.'" size="15" value="'.(($obj->limite != 0)?price($obj->limite):'').'" /> '.$langs->getCurrencySymbol($conf->currency);
			print '</td>';
			// commission rate
			print '<td>';
			print '<input class="right" type="text" name="rate_'.$obj->rowid.'" size="8" value="'.price($obj->rate).'" /> %';
			print '</td>';
			// delete
			print '<td>';
			print '<a href="'.$_SERVER["PHP_SELF"].'?id='.$obj->rowid.'&amp;action=delete&amp;tab=rules">';
			print img_delete();
			print '</a>';
			print '</td>';
			print '</tr>';

			$i++;
		}
		$db->free($resql);
	}
}
else
{
	dol_print_error($db);
	return -1;
}

// espacement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td colspan="7">&nbsp;</td>';
print '</tr>';

// nouvelle règle
/*$var=!$var;
print '<tr class="liste_titre">';
print '<td colspan="6" class="liste_titre">'.$langs->trans('NewRule').'</td>';
print '</tr>';
*/
$var=!$var;
print '<tr '.$bc[$var].'>';
// user
print '<td>';
print $form->select_dolusers('','agentId_',1,'',0,'','');
print '</td>';
// contact type
print '<td>';
print '<select class="flat" name="contactType_">';
print '<option value="-1">&nbsp;</option>';
print '<option value="0">'.$langs->trans('CustomerAgent').'</option>';
foreach($contactTypes as $key=>$value)
{
	print '<option value="'.$key.'">'.$value.'</option>';
}
print "</select>\n";
print '</td>';

print '<td>';
$cate_arbo = $form->select_all_categories('product', null, null, null, null, 1);
print $form->multiselectarray('prodcats_', $cate_arbo, $arrayselected, '', 0, '', 0, '200px');
print '</td>';

print '<td>';
$cate_arbo = $form->select_all_categories(Categorie::TYPE_CUSTOMER, null, null, null, null, 1);
print $form->multiselectarray('custcats_', $cate_arbo, $arrayselected, '', 0, '', 0, '200px');
print '</td>';
// customer
print '<td>';
print $form->select_company('','soc_','s.client = 1', true, 0, 0, array(), 0, 'minwidth100 maxwidth200');
print '</td>';
// product type
print '<td>';
echo $form->select_type_of_lines(-1,'productType_',true,true);
print '</td>';
// commission limit
print '<td>';
print '<input class="right" type="text" name="limite_" size="15" /> '.$langs->getCurrencySymbol($conf->currency);
print '</td>';
// commission rate
print '<td>';
print '<input class="right" type="text" name="rate_" size="8" /> %';
print '</td>';
// submit
print '<td>';
print '</td>';
print '</tr>';
print '</table>';

print '<div class="tabsAction">';
print '<input type="submit" class="button"  value="'.$langs->trans('Validate').'">';
print '</div>';

print '</form>';

?>
<?php
/* Copyright (C) 2012	Christophe Battarel	<christophe.battarel@altairis.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       /htdocs/admin/commissions.php
 *		\ingroup    commissions
 *		\brief      Page to setup advanced commissions module
 */

$res=@include("../../main.inc.php");					// For root directory
if (! $res) $res=@include("../../../main.inc.php");		// For "custom" directory

require_once '../lib/commissions.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/admin.lib.php';

$langs->load("admin");
$langs->load("commissions@commissions");

if (! $user->admin) accessforbidden();

$tab = GETPOST('tab')?GETPOST('tab'):'setup';

/*
 * View
 */

llxHeader('',$langs->trans("CommissionsSetup"));


$linkback='<a href="'.DOL_URL_ROOT.'/admin/modules.php">'.$langs->trans("BackToModuleList").'</a>';
print_fiche_titre($langs->trans("commissionsSetup"),$linkback,'setup');

print '<br/>';

$head = commissions_admin_prepare_head();

dol_fiche_head($head, $tab, $langs->trans("Commissions"), 0, '../img/object_commissions.png', 1);

include_once('./'.$tab.'.inc.php');

llxFooter();
$db->close();
?>

<?php
/* Copyright (C) 2012	Christophe Battarel	<christophe.battarel@altairis.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       /htdocs/admin/commissions.php
 *		\ingroup    commissions
 *		\brief      Page to setup advanced commissions module
 */

if( ! $user->rights->commissions->setup) accessforbidden();

require_once '../lib/commissions.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/admin.lib.php';
require_once(DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php');
require_once(DOL_DOCUMENT_ROOT."/compta/facture/class/facture.class.php");

$langs->load("bills");
$langs->load("commissions@commissions");

/*
 * Action
 */
if (GETPOST('commissionBase'))
{
    if (dolibarr_set_const($db, 'COMMISSION_BASE', GETPOST('commissionBase'), 'string', 0, '', $conf->entity) > 0)
    {
          $conf->global->COMMISSION_BASE = GETPOST('commissionBase');
    }
    else
    {
        dol_print_error($db);
    }
}

if (GETPOST('commissionMethod'))
{
    if (dolibarr_set_const($db, 'COMMISSION_METHOD', GETPOST('commissionMethod'), 'string', 0, '', $conf->entity) > 0)
    {
          $conf->global->COMMISSION_METHOD = GETPOST('commissionMethod');
    }
    else
    {
        dol_print_error($db);
    }
}

if (GETPOST('commissionCumul'))
{
    if (dolibarr_set_const($db, 'COMMISSION_CUMUL', (GETPOST('commissionCumul') == "YES"), 'bool', 0, '', $conf->entity) > 0)
    {
          $conf->global->COMMISSION_CUMUL = (GETPOST('commissionCumul') == "YES");
    }
    else
    {
        dol_print_error($db);
    }
}


if (GETPOST('commissionRestrictToContact'))
{
    if (dolibarr_set_const($db, 'COMMISSION_RESTRICT_TO_CONTACT', (GETPOST('commissionRestrictToContact') == "YES"), 'bool', 0, '', $conf->entity) > 0)
    {
          $conf->global->COMMISSION_RESTRICT_TO_CONTACT = (GETPOST('commissionRestrictToContact') == "YES");
    }
    else
    {
        dol_print_error($db);
    }
}
/*
 * View
 */

print "<br>";

print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("Description").'</td>';
print '<td align="left">'.$langs->trans("Value").'</td>'."\n";
print '<td align="left">'.$langs->trans("Details").'</td>'."\n";
print '</tr>';

$var=true;
$form = new Form($db);

print '<form method="post">';

// COMMISSION BASE (TURNOVER / MARGIN)
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("CommissionBase").'</td>';
print '<td align="left">';
print '<input type="radio" name="commissionBase" value="TURNOVER" ';
if (!isset($conf->global->COMMISSION_BASE) || $conf->global->COMMISSION_BASE == "TURNOVER")
  print 'checked';
print ' />';
print $langs->trans("CommissionBasedOnTurnover");
print '<br/>';
print '<input type="radio" name="commissionBase" value="MARGIN" ';
if (empty($conf->margin->enabled))
  print 'disabled';
elseif (isset($conf->global->COMMISSION_BASE) && $conf->global->COMMISSION_BASE == "MARGIN")
  print 'checked';
print ' />';
print $langs->trans("CommissionBasedOnMargins");
print '</td>';
print '<td>'.$langs->trans('CommissionBaseDetails');
print '<br/>';
print $langs->trans('CommissionBasedOnMarginsDetails');
print '</td>';
print '</tr>';

// espacement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td colspan="3">&nbsp;</td>';
print '</tr>';

// COMMISSION METHOD (ORDER / INVOICE / PAYMENT)
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("CommissionMethod").'</td>';
print '<td align="left">';
print '<input type="radio" name="commissionMethod" value="ORDER" ';
if (isset($conf->global->COMMISSION_METHOD) && $conf->global->COMMISSION_METHOD == "ORDER")
  print 'checked';
print ' />';
print $langs->trans("CommissionBasedOnOrders");
print '<br/>';
print '<input type="radio" name="commissionMethod" value="INVOICE" ';
if (! isset($conf->global->COMMISSION_METHOD) || $conf->global->COMMISSION_METHOD == "INVOICE")
  print 'checked';
print ' />';
print $langs->trans("CommissionBasedOnInvoices");
print '<br/>';
print '<input type="radio" name="commissionMethod" value="PAYMENT" ';
if (isset($conf->global->COMMISSION_METHOD) && $conf->global->COMMISSION_METHOD == "PAYMENT")
  print 'checked';
print ' />';
print $langs->trans("CommissionBasedOnPayments");
print '<br/>';
print '<input type="radio" name="commissionMethod" value="FULLPAYMENT" ';
if (isset($conf->global->COMMISSION_METHOD) && $conf->global->COMMISSION_METHOD == "FULLPAYMENT")
  print 'checked';
print ' />';
print $langs->trans("CommissionBasedOnFullPayments");
print '</td>';
print '<td>'.$langs->trans('CommissionMethodDetails');
print '</td>';
print '</tr>';

// espacement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td colspan="3">&nbsp;</td>';
print '</tr>';

// COMMISSION CUMUL (true / false)
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("CommissionCumul").'</td>';
print '<td align="left">';
print '<input type="radio" name="commissionCumul" value="NO" ';
if (! isset($conf->global->COMMISSION_CUMUL) || empty($conf->global->COMMISSION_CUMUL))
  print 'checked';
print ' />';
print $langs->trans("CommissionWithoutCumul");
print '<br/>';
print '<input type="radio" name="commissionCumul" value="YES" ';
if (isset($conf->global->COMMISSION_CUMUL) && $conf->global->COMMISSION_CUMUL)
  print 'checked';
print ' />';
print $langs->trans("CommissionWithCumul");
print '</td>';
print '<td>'.$langs->trans('CommissionCumulDetails');
print '</td>';
print '</tr>';

// espacement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td colspan="3">&nbsp;</td>';
print '</tr>';

// COMMISSION RESTRICT TO CONTACT (true / false)
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("CommissionRestrictToContact").'</td>';
print '<td align="left">';
print '<input type="radio" name="commissionRestrictToContact" value="YES" ';
if (isset($conf->global->COMMISSION_RESTRICT_TO_CONTACT) && $conf->global->COMMISSION_RESTRICT_TO_CONTACT)
  print 'checked';
print ' />';
print $langs->trans("Yes");
print '<br/>';
print '<input type="radio" name="commissionRestrictToContact" value="NO" ';
if (! isset($conf->global->COMMISSION_RESTRICT_TO_CONTACT) || empty($conf->global->COMMISSION_RESTRICT_TO_CONTACT))
  print 'checked';
print ' />';
print $langs->trans("No");
print '</td>';
print '<td>'.$langs->trans('CommissionRestrictToContactDetails');
print '</td>';
print '</tr>';

// espacement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td colspan="3">&nbsp;</td>';
print '</tr>';

$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td align="center" colspan="3">';
print '<input type="submit" class="button" />';
print '</td>';
print '</tr>';

print '</table>';
print '<br>';

print '</form>';

?>

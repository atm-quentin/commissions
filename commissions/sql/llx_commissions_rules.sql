
--
-- Structure de la table `llx_commissions_rules`
--

CREATE TABLE IF NOT EXISTS `llx_commissions_rules` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) NOT NULL DEFAULT '1',
  `datec` datetime DEFAULT NULL,
  `fk_c_type_contact` int(11) DEFAULT NULL,
  `fk_user` int(11) DEFAULT NULL,
  `fk_product_type` smallint(6) DEFAULT -1,
  `rate` double(6,3) DEFAULT NULL,
  `limite` double(24,8) DEFAULT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB;

ALTER TABLE `llx_commissions_rules` ADD `fk_soc` INT(11) NULL AFTER `fk_user`;
ALTER TABLE `llx_commissions_rules` ADD `fk_prodcat` VARCHAR(255) NULL AFTER `fk_soc`;
ALTER TABLE `llx_commissions_rules` ADD `fk_custcat` VARCHAR(255) NULL AFTER `fk_prodcat`;
